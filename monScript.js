$("document").ready(function () {
    $("#main").load("html/home.html");

    $("#lienHome").click(function () {
        $("#main").fadeOut(function () {
            $("#main").load("html/home.html");
        });
        $("#main").fadeIn();
    })

    $("#lienPage2").click(function () {
        $("#main").fadeOut(function () {
            $("#main").load("html/form.html");
            $.get("https://randomuser.me/api/", function (data) {
                let user = data.results[0];
                console.log($("#Prenom"))
                $("#Prenom").val(user.name.first);
                $("#Nom").val(user.name.last);
                $("#Email").val(user.email);
                $("#Pays").val(user.location.country);
                console.log(user);
            })
        });
        $("#main").fadeIn();
    })



    $("#lienPage3").click(async function () {
        await $("#main").fadeOut();
        await $("#main").load("html/countries.html");
        let lesPays = await $.ajax({
            url: "https://restcountries.eu/rest/v2/all",
            method: "GET",
            dataType: "json"
        })
        for (const country of lesPays) {
            $("#listePays").append(`<option value="${country.name}">`);
        }

        $("#recherchePays").change(async function () {
            let nomPays = $("#recherchePays").val();
            let resultat = await $.ajax({
                url: "https://restcountries.eu/rest/v2/name/" + nomPays,
                method: "GET",
                dataType: "json"
            })
            for (const unPays of resultat) {
                $("#pays").append(`<div class="card" style="width: 18rem; margin:4% 0%;">
                        <img class="card-img-top" src="${unPays.flag}" alt="Card image cap">
                        <div class="card-body">
                          <h5 class="card-title">${unPays.name}</h5>
                          <p class="card-text">Region : ${unPays.region}<br> Capital : ${unPays.capital} <br> Population : ${unPays.population} <br></p>
                        </div>
                      </div>`)
            }
        })
        $("#main").fadeIn();


    })

})